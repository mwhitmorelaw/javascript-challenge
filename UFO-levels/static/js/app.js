// from data.js
const filterBtn = d3.select('#filter-btn');

const filterDateInput = d3.select('#datetime')
const cityInput = d3.select('#city');
const stateInput = d3.select('#state');
const countryInput = d3.select('#country');
const shapeInput = d3.select('#shape');

displayData(ta);

filterBtn.on('click', _ =>{
    // Get the values of all inputs.
    const date = filterDateInput.property('value');
    const city = cityInput.property('value');
    const state = stateInput.property('value');
    const country = countryInput.property('value');
    const shape = shapeInput.property('value');

    displayData(ta, date, city, state, country, shape);
});

function filterCurry(date, city, state, country, shape){
    const vals = [
        ['datetime', date],
        ['city', city],
        ['state', state],
        ['country', country],
        ['shape', shape],
    ];
    // Each property should be looped over, if it is empty no filter will be applied.
    // If it has a value, it will be filtered on the value.
    // Every property must either be empty or equal to value for a given row to be included.
    return d => vals.map(v => emptyOrEqual(d, ...v)).every(v => v);
}

function emptyOrEqual(d, propName, val){
    return val === '' || val === undefined || d[propName].toUpperCase() === val.toUpperCase()
}

function displayData(tableData, date, city, state, country, shape){
    // Wipe out all rows each time, so that old rows don't cumulate
    d3.select('tbody')
        .selectAll('tr')
        .remove();

    // filterCurry will only be called once, per call of displayData
    // It will return the function that will be used to filter.
    const filterFunc = filterCurry(date, city, state, country, shape);

    d3.select('tbody')
        .selectAll('tr')
        .data(tableData)
        .enter()
        .append('tr')
        // filter out nonmatching rows here, so that nonmatching rows do not need to be given td elements.
        .filter(filterFunc)
        .selectAll('td')
        .data(t => [
            t.datetime, 
            capitalize(t.city), 
            t.state.toUpperCase(), 
            t.country.toUpperCase(), 
            capitalize(t.shape), 
            capitalize(t.durationMinutes), 
            t.comments])
        .enter()
        .append('td')
        .text(t => t);
}

function capitalize(s){
    // Need to check if s is in fact a string to avoid index out of bounds error.
    return (!s.charAt) ? '' : s.charAt(0).toUpperCase() + s.slice(1);
}

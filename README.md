# UFO Sightings

This project is intended to organize a data sets consisting of purported UFO sightings. 
It contains HTML, CSS, image, and JavaScript files that can be served as a static website.

## Getting Started

Inside the UFO-Levels folder there is an index.html file. It can be viewed directly by opening it in a web browser. The index.html file will display the search page. 
There are several input fields on the left hand side of the screen that can be used to filter datadisplayed.

### Prerequisites

This project requires only a web browser to run. 

### Installing

No installation is necessary.

## Deployment

The files in this repository can be hosted as a static website. 

## Built With

* [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/) - Styling Framework
* [D3.js](https://d3js.org/) - Data-Driven DOM Manipulation Framework

## Versioning

We use [SemVer](http://semver.org/) for versioning. The version is currently 1.0.0.

## Authors

* **Michael Whitmore** - *All work* - [mwhitmorelaw](https://gitlab.com/mwhitmorelaw)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

